package com.lunamezzogiorno.booklibrary;

import com.lunamezzogiorno.booklibrary.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * A Book Library project based on Spring Boot using spring-boot-starter-data-jpa, lombok, mysql.
 * Author of code: Katlin Kalde 02.12.23
 * Description of the project:
 * The Main class BookLibraryApplication runs the application.
 *
 * Application description:
 * Book entity has rows which are kept in the Book table in MySql database.
 * This entity has an automatically generated identifier of type Long and columns:
 * title, author, ISBN, pagesNum.
 *
 * The library is initialized in service/DbInit
 *
 * repository/LiteratureRepository interface JPA repository has methods:
 *
 * returning all books with a specific title
 * returning the book by ISBN
 * returning the author's book on a specific ISBN
 * a method returning the 3 thinnest books by a given author (the one with the least pages is the first, the last one with the most)
 * a method returning the 3 thickest books by a given author (the one with the most pages is the first, the last one with the least)
 * returning all books whose names begin with a particular String
 * returning all books with pages between the two values
 * returning all books which pages number is less than (a value)
 *
 * These queries are constructed using the appropriate names of JpaRepository methods.
 * In addition, a method called findWherePagesNumIsGreaterThanX with one argument
 * of type Integer, is created
 * that returns all books that have at least as many pages as the input argument is.
 *
 * Methods are called by service/BookService and results are printed out to console.
 */

@SpringBootApplication
public class BookLibraryApplication {
    @Autowired
    BookService bookService;

    public static void main(String[] args) {
        SpringApplication.run(BookLibraryApplication.class, args);
    }

    @Bean
    CommandLineRunner run() {
        return args -> {
            System.out.println("Find by title: " + bookService.findByTitle("Only the Good Spy Young"));
            System.out.println("Find by ISBN: " + bookService.findBookByISBN(1234567890125L));
            System.out.println("Find by author+isbn: " +
                    bookService.findBookByAuthorAndISBN("Ally Carter", 1234567890125L));
            System.out.println("Find by author, order desc by pagenum limit top 3: " +
                    bookService.findByAuthorOrderByPagesNumAsc("Ally Carter", 0, 3));
            System.out.println("Find by author, order desc by pagenum limit top 3: " +
                    bookService.findTop3ByAuthorOrderByPagesNumDesc("Ally Carter"));
            System.out.println("Find by title starting with: " +
                    bookService.findAllByTitleStartingWith("Out"));
            System.out.println("Find by page num between: " +
                    bookService.findAllByPagesNumBetween(55, 65));
            System.out.println("Find by page num less than: " +
                    bookService.findAllByPagesNumIsLessThan(56));
            System.out.println("Find by page num greater than: " +
                    bookService.findWherePagesNumIsGreaterThanX(62));
        };
    }
}
