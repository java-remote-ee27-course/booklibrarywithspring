package com.lunamezzogiorno.booklibrary.repository;

import com.lunamezzogiorno.booklibrary.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LiteratureRepository extends JpaRepository<Book, Long> {
    List<Book> findAllByTitle(String title);
    Book findByISBN(Long isbn);
    Book findByAuthorAndISBN(String Author, Long isbn);
    List<Book> findByAuthorOrderByPagesNumAsc(String author);
    List<Book> findTop3ByAuthorOrderByPagesNumDesc(String author);
    List<Book> findAllByTitleStartingWith(String string);
    List<Book> findAllByPagesNumBetween(Integer startPageNum, Integer endPageNum);
    List<Book> findAllByPagesNumIsLessThan(Integer number);

    @Modifying
    @Query(value = "FROM Book WHERE pagesNum > :number")
    List<Book> findWherePagesNumIsGreaterThanX(Integer number);
}
