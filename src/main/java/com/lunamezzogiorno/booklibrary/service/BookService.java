package com.lunamezzogiorno.booklibrary.service;

import com.lunamezzogiorno.booklibrary.repository.LiteratureRepository;
import com.lunamezzogiorno.booklibrary.entity.Book;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;

@Transactional
@Service
public class BookService {
    private final LiteratureRepository literatureRepository;
    public BookService(LiteratureRepository literatureRepository) {
        this.literatureRepository = literatureRepository;
    }

    public List<Book> findByTitle(String title) {
        return literatureRepository.findAllByTitle(title);
    }

    public Book findBookByISBN(Long isbn) {
        return literatureRepository.findByISBN(isbn);
    }

    public Book findBookByAuthorAndISBN(String author, Long isbn) {
        return literatureRepository.findByAuthorAndISBN(author, isbn);
    }

    public List<Book> findByAuthorOrderByPagesNumAsc(String author, int start, int end) {
        return literatureRepository.findByAuthorOrderByPagesNumAsc(author).subList(start, end);
    }

    public List<Book> findTop3ByAuthorOrderByPagesNumDesc(String author){
        return literatureRepository.findTop3ByAuthorOrderByPagesNumDesc(author);
    }

    public List<Book> findAllByTitleStartingWith(String string) {
        return literatureRepository.findAllByTitleStartingWith(string);
    }

    public List<Book> findAllByPagesNumBetween(Integer start, Integer end) {
        return literatureRepository.findAllByPagesNumBetween(start, end);
    }

    public List<Book> findAllByPagesNumIsLessThan(Integer number) {
        return literatureRepository.findAllByPagesNumIsLessThan(number);
    }

    public List<Book> findWherePagesNumIsGreaterThanX(Integer number) {
        return literatureRepository.findWherePagesNumIsGreaterThanX(number);
    }

    public void deleteAll() {
        literatureRepository.deleteAll();
    }

    public void saveAll(List<Book> books) {
        literatureRepository.saveAll(books);
    }
}
