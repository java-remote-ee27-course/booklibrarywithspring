package com.lunamezzogiorno.booklibrary.service;

import com.lunamezzogiorno.booklibrary.entity.Book;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.Arrays;

/**
 * BookOrders class implements CommandLineRunner to initialize database
 */
@Service
public class DbInit implements CommandLineRunner {
    private final BookService bookService;

    public DbInit(BookService bookService) {
        this.bookService = bookService;
    }


    @Override
    public void run(String... args) throws Exception {

        this.bookService.deleteAll();
        Book book1 = new Book("I'd Tell You I Love You, But Then I'd Have to Kill You",
                "Ally Carter", 1234567890123L, 55);
        Book book2 = new Book("Cross My Heart and Hope to Spy",
                "Ally Carter", 1234567890124L, 58);
        Book book3 = new Book("Don't Judge a Girl by Her Cover",
                "Ally Carter", 1234567890125L, 59);
        Book book4 = new Book("Only the Good Spy Young",
                "Ally Carter", 1234567890126L, 45);
        Book book5 = new Book("Out of Sight, Out of Time",
                "Ally Carter", 1234567890127L, 62);
        Book book6 = new Book("The Silkworm",
                "Robert Galbraith", 3234567890127L, 67);
        Book book7 = new Book("Bridget Jones Diary",
                "Helen Fielding", 2234567890120L, 62);
        Book book8 = new Book("Bridget Jones: The Edge of Reason",
                "Helen Fielding", 2234567890121L, 69);
        Book book9 = new Book("Out of Sight, Out of Time",
                "Mary Louise", 9234567890120L, 55);
        this.bookService.saveAll(Arrays.asList(book1, book2, book3, book4, book5, book6, book7, book8, book9));
    }
}
