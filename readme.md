# A Book Library project 

02.12.23

## Description of the project:
**A Book Library mini-project based on Spring Boot using spring-boot-starter-data-jpa, lombok, mysql. Several queries are made to a MySql book library database and results are printed out to console.**

- The Main class BookLibraryApplication runs the application.
- Book entity rows are kept in the Book table in MySql database.
- This entity has an automatically generated identifier of type Long and columns:
  - title
  - author
  - ISBN
  - pagesNum

- The Book library is initialized in service/DbInit class with dummy demo data
- Interface repository/LiteratureRepository JPA repository has methods:
  - returning all books with a specific title
  - returning the book by ISBN
  - returning the author's book on a specific ISBN
  - a method returning the 3 thinnest books by a given author (the one with the least pages is the first, the last one with the most)
  - a method returning the 3 thickest books by a given author (the one with the most pages is the first, the last one with the least)
  - returning all books whose names begin with a particular String
  - returning all books with pages between the two values
  - returning all books which pages number is less than (a value)

- These queries are constructed using the appropriate names of JpaRepository methods.

In addition, a method called 
- findWherePagesNumIsGreaterThanX with one argument of type Integer, is created that returns all books that have at least as many pages as the input argument is.

All JPA methods are called by service/BookService.  

**The results are printed out to console.**

## Technologies used

- Based on Spring Boot using spring-boot-starter-data-jpa, lombok, mysql. 
- Build tool Maven.

## Project structure

- Main class: BookLibraryApplication
- entity package:
  - Book class
- repository package: 
  - LiteratureRepository interface
- service package:
  - BookService class
  - DbInit class

## Prerequisites to run the code

- Maven
- Optional: Intellij Pro, JDK 20
- Java version 17


## Launch procedure

- git clone this repository locally
- verify that mysql is running on the target system
- Create a mysql database (schema) named: booklibrarydatabase (or you may use the database dump included: BookLibraryDatabaseDump20231203.sql). Or you may set database creation automatically adding to datasource in application.properties: createDatabaseIfNotExist=true, i.e: spring.datasource.url=jdbc:mysql://localhost:3306/booklibrarydatabase?useSSL=false&createDatabaseIfNotExist=true
- copy the file src/main/resources/application.properties.example into src/main/resources/application.properties and provide appropriate options
- the database tables will be created automatically because of the spring.jpa.hibernate.ddl-auto=create
- launch the application, the entry point is: com.lunamezzogiorno.booklibrary.BookLibraryApplication class

![main.png](main.png)

## Author of Code
Katlin Kalde
